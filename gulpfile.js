var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var connect = require('gulp-connect-multi')();

var sassPaths = [
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src('scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(gulp.dest('css'));
});

gulp.task('watch', function (){ 
  gulp.watch(['scss/**/*.scss'], ['sass']);
});

gulp.task('webserver', connect.server({
  root: [__dirname]
}));

gulp.task('copyindex', function () {
  return gulp.src('./index.html')
      .pipe(gulp.dest('./' + 'dist'));
});
gulp.task('copycss', function () {
  return gulp.src('./css/*.*')
      .pipe(gulp.dest('./' + 'dist/css'));
});



gulp.task('default', ['webserver','sass', 'watch']);
gulp.task('dist', ['copyindex', 'copycss']);
